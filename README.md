# README #

This repository contains files for the CircusBot project.

### Our goal ###
* Our goal is to teach the robot to recognise shapes and act accordingly.

### Team members: ###
* Heba Hamed Elshatoury
* Luka Bulatovic
* Liisbet Kangur
* Artjom Sevjan

### Overview ###

For this year�s project our team will be working on the image trained circus robot. Our robot will have to recognise 7 different shapes which are presented as the black contours on a white piece of paper. The shapes, however, cannot be filled. In correspondence with a specific shape, the robot must perform a task that is only specific to a given shape. Some of the tasks that our robot ought to perform are: 1) Driving in a fancy way; 2) Waving a flag by using a servo motor; 3) Scrolling song lyrics over LCD; 4) Doing LCD back light disco. The robot will finish a specific task before being able to process another shape. 

### Pair A: Artjom and Liisbet ###

* 12-18.11      Task: Come up with 7 different tasks for the robot. Start writing the overall code.
* 19-25.11      Task: Be able to perform at least 4 tasks and connect one of them to pair B�s task.
* 26.11-02.12   Task: Finnish off all the remaining tasks for the robot.
* 3-9.12        Task: Work with pair B to create a universal code.
* 10-16.12      Task: Code cleaning and final tests. 

### Pair B: Luka and Heba ###

### Task Distribution for pair B###

* 12-18.11      Task: Choose 7 different shapes to detect. Start writing the overall code
* 19-25.11      Task: Be able to recognise at least 4 shapes and connect one of them to pair A�s task.
* 26.11-02.12   Task: Finnish off all the remaining recognition codes.
* 3-9.12        Task: Work with pair A to create a universal code. Create the poster 
* 10-16.12      Task: Code cleaning and final tests. 

### Challenges and Solutions for pair B ###

During the image processing part, we managed to successfully identify black colour using the HSV values and use the thresholded image to detect the shapes. However, we did encounter a couple of problems that need further addressing.

i)**Problem:** Our code works perfectly on images but when used with a camera it sometimes misjudges the shape. It also finds it hard to differentiate between shapes that have a lot of sharp edges, e.g. a start, octagon etc.
**Solution:** Try other blurs and types of noise removal in order to get a nice image where the contours can be counted properly. Another alternative would be to use so called YOLO method, though, the method seems complex and requires a lot of advanced programming.

ii)**Problem:** Our program gets confused if there is a lot of background noise that we could not find a way of cancelling, e.g. shades on the wall. 
**Solution:** We could give a minimum area to our detection so it would not recognize smaller readings. It would also be useful to calibrate the code in the room where the demo presentation is going to take place. Should the shapes be showed in a totally white background, then, we should not have a lot of problems with the false detection.

iii)**Problem:** The code is running very fast and sometimes when it detects the right shape, for example a square, the output might print square 30times, then it might print a rectangle, and then square again. 
**Solution:** To avoid this we were thinking of introducing a count that would check if we detected the same shape for the last 30 runs, and if so, it will print the shape and exit the code. 


### Component list ###

| Items           | Quantity | Do we need it from you? |
| ----------------| ---------| ------------------------|
| GoPiGo          | 1        | Yes                     |
| RaspberryPi     | 1        | Yes                     |
| Arduino Nano    | 1        | Yes                     |
| USB Camera      | 1        | Yes                     |
| LCD             | 1        | Yes                     |
| RGB LED module  | 1        | Yes                     |
| Servo Motor     | 1        | Yes                     |
| Battery         | 1        | Yes                     |
