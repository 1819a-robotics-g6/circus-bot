#include <Wire.h>
#include <Adafruit_RGBLCDShield.h>
#include <utility/Adafruit_MCP23017.h>
#include <Servo.h>

Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();
// These #defines make it easy to set the backlight color
#define RED 0x1
#define YELLOW 0x3
#define GREEN 0x2
#define TEAL 0x6
#define BLUE 0x4
#define VIOLET 0x5
#define WHITE 0x7

Servo myservo;

// defining row and column pins for ease of use.
int R1 = 2;
int R2 = 3;
int R3 = 4;
int R4 = 5;
int R5 = 6;
int C1 = A0;
int C2 = A1;
int C3 = A2;
int C4 = A3;
int C5 = 7;

int pos = 0;
int i;

int commandNr = 0;

void setup() {


  Serial.begin(9600);
  
  lcd.begin(16, 2);

// setting row and column pins as output pins.
  pinMode(R1, OUTPUT);
  pinMode(R2, OUTPUT);
  pinMode(R3, OUTPUT);
  pinMode(R4, OUTPUT);
  pinMode(R5, OUTPUT);
  pinMode(C1, OUTPUT);
  pinMode(C2, OUTPUT);
  pinMode(C3, OUTPUT);
  pinMode(C4, OUTPUT);
  pinMode(C5, OUTPUT);

// for starters turning all the LED's off in the matrix
  digitalWrite(R1, LOW);
  digitalWrite(R2, LOW);
  digitalWrite(R3, LOW);
  digitalWrite(R4, LOW);
  digitalWrite(R5, LOW);
  digitalWrite(C1, HIGH);
  digitalWrite(C2, HIGH);
  digitalWrite(C3, HIGH);
  digitalWrite(C4, HIGH);
  digitalWrite(C5, HIGH);
}


//Show smile face on dot matrix display
void smileyFace() {
  for (i = 0; i < 8500; i++) {
    digitalWrite(C2, LOW);
    digitalWrite(C3, LOW);
    digitalWrite(C4, LOW);
    digitalWrite(R1, HIGH);
    delay(0.0005);
    digitalWrite(C2, HIGH);
    digitalWrite(C3, HIGH);
    digitalWrite(C4, HIGH);
    digitalWrite(R1, LOW);
  
    digitalWrite(C1, LOW);
    digitalWrite(C5, LOW);
    digitalWrite(R2, HIGH);
    delay(0.0005);
    digitalWrite(C1, HIGH);
    digitalWrite(C5, HIGH);
    digitalWrite(R2, LOW);
  
    digitalWrite(C2, LOW);
    digitalWrite(C4, LOW);
    digitalWrite(R4, HIGH);
    digitalWrite(R5, HIGH);
    delay(0.0005);
    digitalWrite(C2, HIGH);
    digitalWrite(C4, HIGH);
    digitalWrite(R4, LOW);
    digitalWrite(R5, LOW);
  }
  for (i = 0; i < 3000; i++) {
    digitalWrite(C2, LOW);
    digitalWrite(C3, LOW);
    digitalWrite(C4, LOW);
    digitalWrite(R1, HIGH);
    delay(0.0005);
    digitalWrite(C2, HIGH);
    digitalWrite(C3, HIGH);
    digitalWrite(C4, HIGH);
    digitalWrite(R1, LOW);
  
    digitalWrite(C1, LOW);
    digitalWrite(C5, LOW);
    digitalWrite(R2, HIGH);
    delay(0.0005);
    digitalWrite(C1, HIGH);
    digitalWrite(C5, HIGH);
    digitalWrite(R2, LOW);
  
    digitalWrite(C2, LOW);
    digitalWrite(R4, HIGH);
    delay(0.0005);
    digitalWrite(C2, HIGH);
    digitalWrite(R4, LOW);
    
    digitalWrite(C4, LOW);
    digitalWrite(R4, HIGH);
    digitalWrite(R5, HIGH);
    delay(0.0005);
    digitalWrite(C4, HIGH);
    digitalWrite(R4, LOW);
    digitalWrite(R5, LOW);

  }
  for (i = 0; i < 8500; i++) {
    digitalWrite(C2, LOW);
    digitalWrite(C3, LOW);
    digitalWrite(C4, LOW);
    digitalWrite(R1, HIGH);
    delay(0.0005);
    digitalWrite(C2, HIGH);
    digitalWrite(C3, HIGH);
    digitalWrite(C4, HIGH);
    digitalWrite(R1, LOW);
  
    digitalWrite(C1, LOW);
    digitalWrite(C5, LOW);
    digitalWrite(R2, HIGH);
    delay(0.0005);
    digitalWrite(C1, HIGH);
    digitalWrite(C5, HIGH);
    digitalWrite(R2, LOW);
  
    digitalWrite(C2, LOW);
    digitalWrite(C4, LOW);
    digitalWrite(R4, HIGH);
    digitalWrite(R5, HIGH);
    delay(0.0005);
    digitalWrite(C2, HIGH);
    digitalWrite(C4, HIGH);
    digitalWrite(R4, LOW);
    digitalWrite(R5, LOW);
  }
}

//Wave a flag
void waveFlag() {
  myservo.attach(9);
    for (pos = 0; pos <= 90; pos += 1) { // goes from 0 degrees to 180 degrees
      // in steps of 1 degree
      myservo.write(pos);              // tell servo to go to position in variable 'pos'
      delay(15);                       // waits 15ms for the servo to reach the position
    }
    for (pos = 90; pos >= 0; pos -= 1) { // goes from 180 degrees to 0 degrees
      myservo.write(pos);              // tell servo to go to position in variable 'pos'
      delay(15);                       // waits 15ms for the servo to reach the position
    }
    for (pos = 0; pos <= 90; pos += 1) { // goes from 0 degrees to 180 degrees
      // in steps of 1 degree
      myservo.write(pos);              // tell servo to go to position in variable 'pos'
      delay(15);                       // waits 15ms for the servo to reach the position
    }
  myservo.detach();
}

void lcdDisco() {
 
  lcd.setBacklight(RED);
  delay(250);
  lcd.setBacklight(YELLOW);
  delay(250);
  lcd.setBacklight(GREEN);
  delay(250);
  lcd.setBacklight(TEAL);
  delay(250);
  lcd.setBacklight(BLUE);
  delay(250);
  lcd.setBacklight(VIOLET);
  delay(250);
  lcd.setBacklight(WHITE);
  delay(250);
  lcd.setBacklight(RED);
  delay(250);
  lcd.setBacklight(YELLOW);
  delay(250);
  lcd.setBacklight(GREEN);
  delay(250);
  lcd.setBacklight(TEAL);
  delay(250);
  lcd.setBacklight(BLUE);
  delay(250);
  lcd.setBacklight(VIOLET);
  delay(250);
  lcd.setBacklight(WHITE);
  delay(250);
}

void lcdSong() {

  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("It's beginning to look a lot like Christmas Everywhere you go Take a look in the five and ten glistening once again With candy canes and silver lanes aglow It's beginning to look a lot like Christmas Toys in every store But the prettiest sight to see, is the holly that will be On your own front door A pair of hop-along boots and a pistol that shoots Is the wish of Barney and Ben Dolls that will talk and will go for a walk Is the hope of Janice and Jen And Mom and Dad can hardly wait for school to start again It's beginning to look a lot like Christmas Everywhere you go");
  delay(1000);
    for (int positionCounter = 0; positionCounter < 100; positionCounter++) {
    // scroll one position left:
    lcd.scrollDisplayLeft();
    // wait a bit:
    delay(500);
  }
  lcd.clear();
  
}

void loop() {

  if (Serial.available() > 0) {
    commandNr = Serial.read();
      
      // Switch with tasks functions. TO-DO: LCD Display
      switch (commandNr) {
        case 48:
          Serial.println("Waiting for the command!");
          break;
        case 49:
          waveFlag();
          break;
        case 50:
          smileyFace();
          break;
        case 51:
          lcdDisco();
          break;
        case 52:
          lcdSong();
          break;
        
      }
  }
}
