// include the library code:
#include <Wire.h>
#include <Adafruit_RGBLCDShield.h>
#include <utility/Adafruit_MCP23017.h>

int commandNr = 0;
// The shield uses the I2C SCL and SDA pins. On classic Arduinos
// this is Analog 4 and 5 so you can't use those for analogRead() anymore
// However, you can connect other I2C sensors to the I2C bus and share
// the I2C bus.
Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();

// These #defines make it easy to set the backlight color
#define RED 0x1
#define YELLOW 0x3
#define GREEN 0x2
#define TEAL 0x6
#define BLUE 0x4
#define VIOLET 0x5
#define WHITE 0x7

void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("It's beginning to look a lot like Christmas Everywhere you go Take a look in the five and ten glistening once again With candy canes and silver lanes aglow It's beginning to look a lot like Christmas Toys in every store But the prettiest sight to see, is the holly that will be On your own front door A pair of hop-along boots and a pistol that shoots Is the wish of Barney and Ben Dolls that will talk and will go for a walk Is the hope of Janice and Jen And Mom and Dad can hardly wait for school to start again It's beginning to look a lot like Christmas Everywhere you go There's a tree in the Grand Hotel, one in the park as well The sturdy kind that doesn't mind the snow It's beginning to look a lot like Christmas Soon the bells will start And the thing that will make them ring, is the carol that you sing Right within your heart It's beginning to look a lot like Christmas Soon the bells will start And the thing that will make them ring, is the carol that you sing Right within your heart It's beginning to look a lot like Christmas, Christmas, Christmas, Christmas, Christmas");
  delay(1000);
    for (int positionCounter = 0; positionCounter < 1088; positionCounter++) {
    // scroll one position left:
    lcd.scrollDisplayLeft();
    // wait a bit:
    delay(150);
  }
}

void loop() {

  }
