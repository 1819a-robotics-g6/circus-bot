#Feel free to edit this file! This is just an example without commputer vision.
import rasPiTasks

def taskCaller(taskId):
    if taskId == '1':
        rasPiTasks.waveFlag()
    elif taskId == '2':
        rasPiTasks.dotMatrix()
    elif taskId == '3':
        rasPiTasks.lcdDisco()
    elif taskId == '4':
        rasPiTasks.lcdSong()
    elif taskId == '5':
        rasPiTasks.square()
    elif taskId == '6':
        rasPiTasks.catwalk()


while True:
    command = input('Enter command: ')
    
    if command == '0':
        break
    else:
        taskCaller(command)