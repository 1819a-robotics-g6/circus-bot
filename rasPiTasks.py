import gopigo as go
from gopigo import *
import time
import serial

go.set_speed(50)

#Additional setup for GoPiGo, if necessary
go.set_right_speed(45)
go.set_left_speed(50)

#Opening serial connection with Arduino
port = '/dev/ttyUSB0' #May be different
ser = serial.Serial(port, 9600, timeout=0)

#GoPiGo settings
def forward(t):
	go.fwd()
	time.sleep(t)
	go.stop()

def backward(t):
	go.bwd()
	time.sleep(t)
	go.stop()

def left(t):
	go.left()
	time.sleep(t)
	go.stop()

def right(t):
	go.right()
	time.sleep(t)
	go.stop()


#Arduino tasks

def waveFlag():
    ser.write(b'1')
    
def dotMatrix():
    ser.write(b'2')

def lcdSong():
    ser.write(b'3')

def lcdDisco():
    return

#Raspberry Pi GoPiGO tasks

def square():
    forward(3)
    right_rot()
    time.sleep(1.5)
    forward(3)
    right_rot()
    time.sleep(1.5)
    forward(3)
    right_rot()
    time.sleep(1.5)
    forward(3)
    
def catwalk():
    forward(0.5)
    
    right_rot()
    time.sleep(0.1)
    
    forward(0.5)
    
    left_rot()
    time.sleep(0.1)
    
    forward(0.5)
    
    right_rot()
    time.sleep(0.1)
    
    forward(0.5)
    
    left_rot()
    time.sleep(0.1)
    
    forward(0.5)
    
    forward(0.5)
    
    right_rot()
    time.sleep(0.1)
    
    forward(0.5)
    
    left_rot()
    time.sleep(0.1)
    
    forward(0.5)
    
    right_rot()
    time.sleep(0.1)
    
    forward(0.5)
    
    left_rot()
    time.sleep(0.1)
    
    forward(0.5)
